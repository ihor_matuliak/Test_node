module.exports = {
    port: 8080,
    jwtSecret: 'secret',
    mongoUri: 'mongodb+srv://ihor:igor123@cluster0.wzqfq.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
    awsBucketName: 'bucket-name',
    awsAccessKeyId: 'awsAccessKeyId',
    awsSecretAccessKey: 'awsSecretAccessKey',
    awsRegion: 'aws-region',
    awsUploadedFileUrlLink: 'https://aws-region.amazonaws.com/bucket-name/',
    emailAddress: 'user@example.com',
    emailPassword: 'password',
};
